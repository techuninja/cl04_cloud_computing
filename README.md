# Project Title
CL04 - Cloud Computing

## Pantallazos de aplicación desplegada desde AWS usando herrramienta Postman
Url: https://docs.google.com/document/d/14zG4XrqPr-VO4EvzCjUVoS3RPBHl7PRNHVNoxVSEuGU/edit?usp=sharing

## Link a validar que devuelve todos los productos desde AWS
### Por temas de consumo de la capa gratuita, solo estara disponible la url un tiempo corto

### Consulta por lista de productos
Metodo GET

Url: http://techuproductosdb.us-west-2.elasticbeanstalk.com:80/apitechu/v2/productos

### Consulta por id de producto
Metodo GET

Url: http://techuproductosdb.us-west-2.elasticbeanstalk.com:80/apitechu/v2/productos/5fcdc9757e5c4b78eb6be180

### Alta de un producto
Metodo POST

Url: http://techuproductosdb.us-west-2.elasticbeanstalk.com:80/apitechu/v2/productos

Request Body:

{
    "descripcion": "Enjuage bucal",
    "precio": 50.1
}

### Actualizacion de un producto
Metodo PUT

Url: http://techuproductosdb.us-west-2.elasticbeanstalk.com:80/apitechu/v2/productos

Request Body:

{
    "id": "5fcddfba31178a540dcef13d",
    "descripcion": "Enjuage bucal",
    "precio": 30.9
}

### Borrado de un producto
Metodo DELETE

Url: http://techuproductosdb.us-west-2.elasticbeanstalk.com:80/apitechu/v2/productos

Request Body:

{
    "id": "5fcddfba31178a540dcef13d",
    "descripcion": "Enjuage bucal",
    "precio": 30.9
}